defmodule MergeSort do

  def sort_memoriy(x, memory) do
    case :ets.lookup(memory, x) do
      [ {_, r} | _] -> r  
      [] -> 
        IO.inspect x
        r = sort(x, memory)
        :ets.insert(memory, {x, r})
        r
    end
  end


  def sort([],_), do: []
  def sort([x],_), do: [x]
  def sort(l,memory) do

    f = Enum.take_every(l,2)
    s = Enum.drop_every(l,2)
    merge(sort_memoriy(f, memory), sort_memoriy(s, memory), [])
  end

  def merge(f, [], r), do: r ++ f
  def merge([], s, r), do: r ++ s
  def merge(f = [fh | ft], s = [sh | st], r) do
    cond do
      fh < sh -> merge(ft, s, r ++ [fh])
      true -> merge(f, st, r ++ [sh])
    end
  end
end