defmodule ServerWeb.PageController do
  use ServerWeb, :controller

  def init(conn, %{"attributes" => attributes, "class" => class}) do 
    id = SecureRandom.uuid
    {:ok, pid} = GenServer.start_link(RemoteClass, [attributes, get_class(class)], name: String.to_atom(id))
    send_response_json(conn, %{pid: id})
  end

  def execute_method(conn, %{"pid" => pid, "function" => function, "args" => args}) do
    return = RemoteClass.execute(String.to_atom(pid), function, args)
    send_response_json(conn, %{return_value: return})
  end

  def get_class(string) do
    ("Elixir._"<>string<>"_class")
    |> String.split("_")
    |> Enum.map(&String.capitalize/1)
    |> Enum.join()
    |> String.to_atom()
  end

  defp send_response_json(conn, map) do
    response = Poison.encode!(map)
    send_resp(conn, 200, response)
  end

  def index(conn, _params) do
    render conn, "index.html"
  end
end
