defmodule HandleFibClass do


  def init(state) do
    if :ets.info(:memorization_fib) == :undefined, do: IO.inspect :ets.new(:memorization_fib, [:named_table, :public,  read_concurrency: true,
  write_concurrency: true])
    state
  end

  def get_last_element_fib(state, list) do
    { 
      state,
    list
    |> List.last()
    |> fib_memoriy()
  }
  end


  def fib_memoriy(x) when x<=0, do: 1
  def fib_memoriy(1), do: 2
  def fib_memoriy(x) do
    case :ets.lookup(:memorization_fib, x) do
      [ {_, r} | _] -> r  
      [] -> 
        IO.inspect x
        r = fib(x)
        :ets.insert(:memorization_fib, {x, r})
        r
    end


  end 

  defp fib(x), do: fib_memoriy(x-1) + fib_memoriy(x-2)

end