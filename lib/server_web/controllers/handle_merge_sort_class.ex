defmodule HandleMergeSortClass do
    @memorization_sort :memorization_sort
  def init(state) do
    if :ets.info(@memorization_sort) == :undefined, do: IO.inspect :ets.new(@memorization_sort, [:named_table, :public,  read_concurrency: true,
  write_concurrency: true])
    state
  end


  def sort(state, array)do
    {
    state,
    MergeSort.sort_memoriy(array, @memorization_sort)
    }
  end

end